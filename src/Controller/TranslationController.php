<?php

namespace TG\Translation\Controller\Translation;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use View;
use TG\Translation\App\TranslationApi;

/**
 * Controller for translation
 * @author Jan Dolata <j.dolata@holonglobe.com>
 */
class TranslationController extends Controller
{

    function __construct()
    {
        View::addLocation('TG\Translation\View');
    }

    /**
     * Show list of all or selected translation
     */
    public function get($file = 'all')
    {
        $data = (new TranslationApi)->get($file);
        return view('index', $data);
    }

    /**
     * Save translation
     * @param  Request $request
     */
    public function save(Request $request)
    {
        (new TranslationApi)->save($request);
        return redirect()
            ->route('translation_get', ['file' => $request->input('file')])
            ->with('info', trans('api.done'));
    }

    /**
     * Show Log
     */
    public function log()
    {
        $data = (new TranslationApi)->log();
        return view('log', $data);
    }
}
