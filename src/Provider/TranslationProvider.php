<?php

namespace TG\Translation\Provider;

use Illuminate\Support\ServiceProvider;

class TranslationProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        
    }

    public function boot(DispatcherContract $events)
    {
        
    }
}